
public class test2 {
	private String motAnglais;
	private String motFrancais;
	private String type;
	private String description;
	
	// Objet Mot avec son mot anglais/francais, son type et sa description.
	public Mot(String motAnglais, String motFrancais, String type, String description) {
		this.motAnglais = motAnglais;
		this.motFrancais = motFrancais;
		this.type = type;
		this.description = description;
	}
	
	public String getMotAnglais() {
		return this.motAnglais;
	}
	public String getmotFrancais() {
		return this.motFrancais;
	}
	public String getType() {
		return this.type;
	}
	public String getDescription() {
		return this.description;
	}
}
