import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Metrics {

    /** Charge un fichier texte en String utilisable dans Java
     * @param file le nom du fichier à importer
     * @return Une string qui contient tout le code du fichier
     */
    public String loadFile(String file) {
        try {
            return Files.readString(Paths.get(file));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /** Passer d'une seul String à un array qui contient les méthodes
     * On split le code par les keywords (public|private...)
     * et ensuite on cherche pour une parenthese.
     * C'est pas exact, mais une facon simple et satisfaisante
     * @param file Le nom du fichier
     * @return Un array de string séparé par méthode
     */
    public ArrayList<String> parseMethod(String file) {

        ArrayList<String> methodList = new ArrayList<>();

        String notParsedFile = loadFile(file);

        String[] parsedMethod = notParsedFile.split("(public|protected|private|static)");
        for (String s : parsedMethod) {
            if (s.contains(")")) {
                methodList.add(s);
            }
        }
        return methodList;
    }

    /** On calcule la complexité d'une méthode. On utilise les prédicates pour compter
     * les différents branchements.
     * On cherche les prédicats pour déterminer les branchements.
     * @param ParsedMethod Une méthode à évaluer
     * @return un integer qui nous donnes la complexité, minimum 1
     */
    public int mesureWMC(String ParsedMethod) {
        String[] predicate = {"switch", "while", "if", "for", "case"};
        int CC = 1;
        String[] s = ParsedMethod.split("\n");
        for (String value : s) {
            for (String item : predicate)
                if (doesContain(value, item)) {
                    CC += 1;
                    break;
                }
        }
        return CC;
    }

    /** Calcul la somme des complexité pour une classe
     * @param methodList un arrayList de String qui contient des méthodes
     * @return un integer qui donne la somme de la complexité
     */
    public int calculateClassWMC(ArrayList<String> methodList) {
        int sumCC = 0;
        for (String s : methodList) {
            sumCC += mesureWMC(s);
        }
        return sumCC;

    }

    /** Calcul du metric WMC
     * @param file nom du fichier à évaluer
     * @return La complexité d'une classe Java
     */
    public int classe_WMC(String file) {

        ArrayList<String> parsedClass = parseMethod(file);
        return calculateClassWMC(parsedClass);
    }

    /** Déterminer si on trouve un mot dans un texte
     * @param source Le texte qu'on veut analysé
     * @param subItem Le mot qu'on veut trouvé
     * @return Boolean, oui si on a le mot dans le texte
     */
    private static boolean doesContain(String source, String subItem){
        if (source.equals("") ){
            return false;
        }
        String pattern = "\\b"+subItem+"\\b";
        Pattern p= Pattern.compile(pattern);
        Matcher m=p.matcher(source.toLowerCase(Locale.ROOT));
        return m.find();
    }

    /** On calcul la complexité pour toutes les classes Java
     * @param filePath Localisation relative du fichier avec le paquet
     * @return integer de la complexité du paquet Java
     */
    public int paquet_WCP(String filePath) {
        int countPaquet_PMC = 0;

        try {
            //on ajoute tous les path de fichiers .java dans un array
            String[] fullPaquetClass = listFilesUsingFileWalk(filePath, 10);

            for (String s : fullPaquetClass) {
                //load le fichier java
                String classCode = Files.readString(Path.of(s), StandardCharsets.UTF_8);
                //on compte le nombre de lignes et lajoute au total
                ArrayList<String> parsedCode = parseClassMethod(classCode);
                countPaquet_PMC += calculateClassWMC(parsedCode);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return countPaquet_PMC;
    }

    /** On touve les méthodes des classes dans le paquet
     * @param file Location relative du paquet
     * @return Un ArrayList des méthodes dans les classes
     * @throws IOException
     */
    public ArrayList<String> parseClassMethod(String file) throws IOException {
        ArrayList<String> methodList = new ArrayList<>();

        String[] parsedMethod = file.split("(public|protected|private|static)");
        for (String s : parsedMethod) {
            if (s.contains(")")) {
                methodList.add(s);
            }
        }
        return methodList;
    }

    /** On walk dans les dossiers et sous-dossier pour trouver tous les fichiers Java
     * @param dir La position du paquet
     * @param depth La profondeur de la recherche
     * @return un String array avec la position de tous les fichiers java
     * @throws IOException
     */
    public String[] listFilesUsingFileWalk(String dir, int depth) throws IOException {
        try (Stream<Path> stream = Files.walk(Paths.get(dir), depth)) {

            return stream
                    .filter(file -> !Files.isDirectory(file))
                    .map(Path::toString) //modif du code source pour seulement garder les fichier java
                    .filter(s -> s.endsWith(".java") )
                    .toArray(String[]::new);
        }
    }
}
