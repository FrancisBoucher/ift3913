import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class commentParser {


    /** On lit le fichier demandé et retourne le nombre de ligne
     * @param input La position relative du fichier à lire
     * @return Integer, le nombre de ligne de code
     * @throws IOException
     */
    public int classe_LOC(String input) throws IOException{
    	String fullReadCode = Files.readString(Path.of(input), StandardCharsets.UTF_8);
    	return this.code_LOC(fullReadCode);

    }

    /** Parse le texte et ensuite calcule le nombre de ligne non vide
     * @param code Le code à évaluer en un String
     * @return Le nombre de ligne non vide
     */
    public int code_LOC(String code){

        //On parse le texte dans un array, chaque ligne est une case dans larray
        String[] parsedTextLines = code.split("\n");

        int EmptyLine = 0;
        //On compte le nombre de ligne vide
        for ( int i = 0; i < parsedTextLines.length - 1; i++) {
            if (parsedTextLines[i].matches("")) {
                EmptyLine++;
            }
        }
        //On soustrait le nombre de ligne vide au nombre de ligne total
        return parsedTextLines.length - EmptyLine;
    }


    /** On trouve tous les fichiers java et on compte le nombre de ligne
     * @param filePath La position relative du paquet à lire
     * @return Int, le nombre de ligne de code dans le paquet
     */
    public int paquet_LOC(String filePath) {
        int countPaquet_LOC = 0;

        try {
            //on ajoute tous les path de fichiers .java dans un array
            String[] fullText = listFilesUsingFileWalk(filePath, 10);

            for (String s : fullText) {
                //load le fichier java
                String fullReadCode = Files.readString(Path.of(s), StandardCharsets.UTF_8);
                //on compte le nombre de lignes et lajoute au total
                countPaquet_LOC += code_LOC(fullReadCode);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return countPaquet_LOC;
    }


    /** On calcul le nombre de ligne de commentaire dans le paquet
     * @param filePath La position relative du paquet à lire
     * @return integer, le nombre de ligne de commentaire dans le paquet
     */
    public int paquet_CLOC(String filePath) {
        int countPaquet_CLOC = 0;

        try {
            //on ajoute tous les path de fichiers .java dans un array ***Il faut changer le depth pour aller profond*** je lest set a 1 pour tester
            String[] fullText = listFilesUsingFileWalk(filePath, 10);

            for (String s : fullText) {
                //on compte le nombre de lignes et lajoute au total
                countPaquet_CLOC += classe_CLOC(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return countPaquet_CLOC;
    }

    /** On calcul le nombre de ligne de commentaire dans une classe
     * @param filePath La position relative de la classe à lire
     * @return Int, le nombre de ligne de commentaire dans une classe
     */
    public int classe_CLOC(String filePath) {
        int count = 0;
        try {
            // On load le fichier et ensuite on le lit ligne par ligne
            String currentLine;
            BufferedReader file = new BufferedReader(new FileReader(filePath));
            LineNumberReader line = new LineNumberReader(file);
            int numberline = 0;
            //On regarde si la ligne contient du commentaire
            while ((currentLine = line.readLine()) != null) {
                numberline++;
                if (currentLine.contains("/*")) {
                    while ((currentLine = line.readLine()) != null) {
                        count++;
                        if (currentLine.contains("*/")) {
                            count++;
                            break;
                        }
                    }
                } else if (currentLine.contains("//")) {
                    count++;
                }

            }

        } catch (FileNotFoundException e) {
            System.out.println("File Not Found.");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;

    }

    /**On walk dans les dossiers et sous-dossier pour trouver tous les fichiers Java
     * @param dir La position du paquet
     * @param depth La profondeur de la recherche
     * @return un String array avec la position de tous les fichiers java
     * @throws IOException
     */
public String[] listFilesUsingFileWalk(String dir, int depth) throws IOException {
    try (Stream<Path> stream = Files.walk(Paths.get(dir), depth)) {

        return stream
                .filter(file -> !Files.isDirectory(file))
                .map(Path::toString)
                .filter(s -> s.endsWith(".java") ) //On regarde juste les fichiers .java
                .toArray(String[]::new);
    }
}

    /** On calcul la densité des commentaires dans un paquet
     * @param filePath La position relative de la classe à lire
     * @return Double, le ratio commentaire sur ligne de code
     */
public double paquet_DC(String filePath){
            double x = paquet_CLOC(filePath);
            double y = paquet_LOC(filePath);

    return (x/y);

    }

    /** On calcul la densité des commentaires dans une classe
     * @param filePath  La position relative de la classe à lire
     * @return Double, le ratio commentaire sur ligne de code
     * @throws IOException
     */
public double classe_DC(String filePath) throws IOException {
        double x = classe_CLOC(filePath);
        String fullReadCode = Files.readString(Path.of(filePath), StandardCharsets.UTF_8);
        double y = code_LOC(fullReadCode);
        return (x/y);
    }
}

