import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class main {

	public static void main(String[] args) {
		
		// Si l'argument pour le path du dossier � �valuer est vide, on prend le dossier "code".
        String filePath = args.length > 0 ? args[0] : System.getProperty("user.dir") + "\\code";
        
        // Initialisation du CommentParser et Metrics
        commentParser cp = new commentParser();
        Metrics metrics = new Metrics();
        
        // Tableau de donn�es sur les classes.
        List<String[]> dataClassLines = new ArrayList<>();
        dataClassLines.add(new String[] 
          { "chemin", "class", "classe_LOC", "classe_CLOC", "classe_DC", "classe_WMC", "classe_BC" });
        
        // Tableau de donn�es sur les paquets.
        List<String[]> dataPackagesLines = new ArrayList<>();
        dataPackagesLines.add(new String[] 
          { "chemin", "paquet", "paquet_LOC", "paquet_CLOC", "paquet_DC", "paquet_WCP", "paquet_BC" });
        
        // Initialisation du csvWriter.
        csvWriter writer = new csvWriter();
        
        // Traitement
        try {
        	// Profondeur maximal de la recherche de fichiers : 15 ********** Pourrait etre augment�e **************
			String[] paths = cp.listFilesUsingFileWalk(filePath, 15);
			
            for (String p : paths) {
            	
            	// Traitement pour Chacun des paths retrouv�.
                try {
                	
                	// Nom de la classe selon le path de la classe
                	String className = p.substring(p.lastIndexOf('\\') + 1, p.lastIndexOf(".java"));
       
                	// Ajout d'une ligne dans le tableau avec les donn�es sur la classe.
                	dataClassLines.add(new String[] 
        			          { p, className, "" + cp.classe_LOC(p), "" + cp.classe_CLOC(p), "" + cp.classe_DC(p),
        			        		  "" + metrics.classe_WMC(p), "" + (cp.classe_DC(p) / metrics.classe_WMC(p)) });
        			
                	// Trouver le path du paquet + nom du paquet
        			File fileJavaClass = new File(p);
        			String packageName = fileJavaClass.getParentFile().getName();
        			String packagePath = p.substring(0, p.lastIndexOf(packageName) + packageName.length());
        			
        			// V�rifier qu'on se retrouve bien dans un sous-dossier.
        			if (!packageName.equals(filePath.substring(filePath.lastIndexOf('\\') + 1))) {
        				// Si le paquet n'a pas deja ete analyse, alors faire l'analyse. [1]
        				boolean packageDejaFait = false;
        				for (int i = 0; i < dataPackagesLines.size(); i++) {
        					packageDejaFait = dataPackagesLines.get(i)[0].equals(packagePath) ? true : packageDejaFait;
        				}
        				
        				// -> [1] : Ajout d'une ligne dans le tableau avec les donn�es sur le paquet.
        				if (!packageDejaFait) {
            				String[] packageLine = {
            						packagePath,
            						packageName,
            						"" + cp.paquet_LOC(packagePath),
            						"" + cp.paquet_CLOC(packagePath),
            						"" + cp.paquet_DC(packagePath),
            						"" + metrics.paquet_WCP(packagePath),
            						"" + (cp.paquet_DC(packagePath) / metrics.paquet_WCP(packagePath))
            				};
        					dataPackagesLines.add(packageLine);
        				}
        			}
        			
        		} catch (IOException e1) {
        			// TODO Auto-generated catch block
        			e1.printStackTrace();
        		}
            }
            
            // Imprimer les tableaux de donn�es vers classes.csv et paquets.csv
            writer.writeCSVFromDataArray("data\\classes.csv", dataClassLines);
            writer.writeCSVFromDataArray("data\\paquets.csv", dataPackagesLines);
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	}

}
