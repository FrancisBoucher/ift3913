**Philip Lelievre 20180931 et Francis Boucher 20116745**

## Lien vers le repositoire BitBucket

https://bitbucket.org/FrancisBoucher/ift3913


## Comment executer le programme 

**Avec paramètre : **

1. Exécuter : java jar tp1_ift3913.jar [chemin]
où chemin est le chemin du dossier à analyser (Exemple : C:\Users\phili\Desktop\IFT3913).
2. Voir les résultats dans le dossier "data" (classes.csv et paquets.csv)

**Ou sans paramètre : **

1. Mettre le code qui doit être analyser dans le dossier "code" (dans le même répertoire que tp1_ift3913.jar)
2. Exécuter : java jar tp1_ift3913.jar
3. Voir les résultats dans le dossier "data" (classes.csv et paquets.csv)

**Exécution Développeur (Dans le projet Java et non le .jar) **

1. Exécuter le src/main.java avec (ou non) comme paramètre le chemin désiré (sur l'IDE Eclipse : bouton run vert -> Run Configurations -> Arguments -> Program arguments).

## Répertoires

/code et /data -> répertoires pour des inputs/outputs éventuelles du programme.
/IFT3913_TP1_Philip_Frank -> Le projet Java.
/partie4 -> Données sur la partie 4 du devoir.
tp1_ift3913.jar -> jar exécutable du programme Java.